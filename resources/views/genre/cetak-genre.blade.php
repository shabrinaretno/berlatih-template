@extends('layout.master')
@section('title')
   Cetak
@endsection

@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        table.static{
            position: relative;
            border: 1px solid #543535
        }
    </style>
    <title>Cetak Data</title>
</head>

<body>
    <div class="form-grup">
        <p align="center"><b>Laporan Data</b></p>
        <table class="static" align="center" rules="all" border="1px" style="width : 95%">
        
        </table>
    </div>
</body>
</html>
@endsection