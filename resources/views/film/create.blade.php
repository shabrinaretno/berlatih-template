@extends('layout.master')
@section('title')
    Tambah Film
@endsection

@section('content')
    <form action="/film" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="judul">Judul Film</label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul Film">
            @error('judul')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="ringkasan">Ringkasan</label>
            <textarea class="form-control" name="ringkasan" id="ringkasan" cols="30" rows="10" placeholder="Masukkan Ringkasan"></textarea>
            @error('ringkasan')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tahun">Tahun</label>
            <input type="number" class="form-control" name="tahun" id="tahun" placeholder="Masukkan Tahun">
            @error('tahun')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="genre">Genre</label>
            <select class="custom-select" name="genre_id" id="genre">
                <option value="">---Pilih Genre---</option>
                @foreach ($listgenre as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
            @error('genre')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="poster">Poster</label>
            <input type="file" class="form-control" name="poster" id="poster">
            @error('poster')
                <div class="alert alert-danger mt-2">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection